import { Room } from "../types";
import { onRoomsUpdate, onJoinRoom, onRoomUpdate, onBotCommentSet } from "../handlers/room-update";
import { leaveRoom } from "../handlers/room-interact";

// storing function, dom element and socket here is not good idea, but 
// it should be shared across different modules of application
export class AppState {
  username?: string;
  activeRoom?: Room;
  joined?: boolean;
  rooms?: { userCount: number, name: string }[] = [];
  text?: string;
  botComment?: string;

  socket?: SocketIOClient.Socket;
  handlerFunc?: (event: KeyboardEvent) => void;
  textElement?: HTMLElement;
}

export const appState = new AppState;

export const proxyState = new Proxy(appState, {
  // get: function(target: AppState, property: keyof AppState){ console.log(target, property); return target[property] },
  set: function(target: AppState, property: keyof AppState, value: any){ 
    target[property] = value;
    if (property === 'rooms') {
      onRoomsUpdate();
    }
    if (property === 'joined') {
      if (value) {
        onJoinRoom();
      }
      if(!value) {
        leaveRoom();
      }
    }
    if (property === 'activeRoom') {
      onRoomUpdate();
    }
    if (property === 'botComment') {
      onBotCommentSet();
    }
    return true 
  }
});