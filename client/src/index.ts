import { onUserStateUpdate } from './handlers/room-update';
import { handleTimerStart, handleChallengeStart, handleChallengeEnd, leaveRoom } from './handlers/room-interact';
import { GameResult, User, Room } from './types/index';
import { handleUserNameNotUnique } from './handlers/error-handlers';
import { actions } from './constants/actions';
import { appState, proxyState } from './state';

if (window.location.pathname === '/login') {
  const username: string | null = sessionStorage.getItem('username');

    if (username) {
        window.location.replace('/game');
    }

    const submitButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById('submit-button');
    const input: HTMLInputElement = <HTMLInputElement>document.getElementById('username-input');

    const getInputValue = () => input?.value;

    const onClickSubmitButton = () => {
        const inputValue: string | null = getInputValue();
        if (!inputValue) {
            return;
        }
        sessionStorage.setItem('username', inputValue);
        window.location.replace('/game');
    };

    const onKeyUp = (ev: KeyboardEvent) => {
        const enterKeyCode: number = 13;
        if (ev.keyCode === enterKeyCode) {
            submitButton?.click();
        }
    };

    submitButton?.addEventListener('click', onClickSubmitButton);
    window.addEventListener('keyup', onKeyUp);
} else {
  
  appState.username = sessionStorage.getItem('username')!;

  if (!appState.username) {
      window.location.replace('/login');
  };

  const socket: SocketIOClient.Socket = io('', { query: { username: appState.username } });
  appState.socket = socket;
  
  socket.on(actions.roomNameNotUnique, (message: string) => window.alert(message));
  socket.on(actions.userNameNotUnique, (message: string) => handleUserNameNotUnique(message))
  socket.on(actions.updateRooms, (rooms: { userCount: number, name: string }[]) => proxyState.rooms = rooms);
  socket.on(actions.userJoined, (room: Room) => {proxyState.joined = true; proxyState.activeRoom = room});
  socket.on(actions.activeRoomUpdate, (room: Room) => proxyState.activeRoom = room);
  socket.on(actions.userStateUpdate, (user: User) => onUserStateUpdate(user));
  socket.on(actions.usersReady, (data: { seconds: number, textIdx: number }) => handleTimerStart(data))
  socket.on(actions.startChallenge, (allowedSeconds: number) => handleChallengeStart(allowedSeconds));
  socket.on(actions.endChallenge, (gameResult: GameResult[]) => handleChallengeEnd(gameResult));
  socket.on(actions.botMessage, (comment: string) => proxyState.botComment = comment)

  const onClickCreateRoom = () => {
      const roomName = prompt('Please, input room name ');
      if (roomName) {
          socket.emit(actions.createRoom, roomName);
      }
  };

  const onClickReady = (event: MouseEvent) => {
    const button: HTMLElement = <HTMLElement>event.target;
    const user: User = appState.activeRoom!.users!.find(user => user.name === appState.username)!;
    if (user) {
      const data = { isReady: !user.progress.isReady, inputtedText: '' };
      socket.emit(actions.toggleReady, data);
      button.textContent = data.isReady ? 'Not ready' : 'Ready';
    }
  }

  const onClickLeaveRoom = () => {
    proxyState.joined = false;
  }

  const createRoomButton: HTMLElement = <HTMLElement>document.getElementById('create-room-button');
  createRoomButton.addEventListener('click', onClickCreateRoom);

  const leaveRoomButton: HTMLElement = <HTMLElement>document.querySelector('.game-page__back-to-rooms-button');
  leaveRoomButton.addEventListener('click', onClickLeaveRoom);

  const readyButton: HTMLElement = <HTMLElement>document.querySelector('.ready-column__ready-button');
  readyButton.addEventListener('click', onClickReady);
}