export interface Room {
  name: string;
  text?: string;
  usersCount?: number;
  users?: User[];

  maxUsersReached?: boolean;
  state?: 'pending' | 'timer_started' | 'game_started';
}

export interface Progress {
  isReady: boolean;
  inputtedText: string;
  doneInPercents: number;
  spentSeconds: number;
}

export interface User {
  name: string;
  progress: Progress;
}

export interface GameResult {
  username: string,
  spentSeconds: number | null
}