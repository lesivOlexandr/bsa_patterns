import { ActionsService } from "./actions.service";
import { Socket } from "socket.io";
import { MessageFactory, messageFactory } from "./message.service";
import { actions } from "../types/actions";
import { messageTypes } from "../types/message-types";
import { User, Room, GameResult } from "../types";
import { DataStorage, dataStorage } from "./data-storage";

export class CommentatorService {
  private actionService: ActionsService;
  private messageService: MessageFactory = messageFactory;
  private storage: DataStorage = dataStorage;

  constructor(socket: Socket){
    this.actionService = new ActionsService(socket);
  }

  onIntroduce(user: User): void {
    const message: string = this.messageService.get(messageTypes.introduction, user);
    this.actionService.emitToUser(actions.botMessage, message);
  }

  onStart(room: Room): void {
    const message: string = this.messageService.get(messageTypes.start, room);
    this.actionService.emitToRoom(actions.botMessage, room, message);
    const intervalId: NodeJS.Timeout = setInterval(() => {
      this.onIntervalEnd(room);
    }, 30_000);
    this.storage.setRoomBotInterval(room, intervalId);
  }
  
  onIntervalEnd(room: Room): void {
    const message: string = this.messageService.get(messageTypes.regular, room);
    this.actionService.emitToRoom(actions.botMessage, room, message);
  }

  onNearFinish(user: User, room: Room): void {
    const message: string = this.messageService.get(messageTypes.nearingFinish, user);
    this.actionService.emitToRoom(actions.botMessage, room, message);
  }

  onFinish(user: User, room: Room): void {
    const message: string = this.messageService.get(messageTypes.finish, { user, room });
    this.actionService.emitToRoom(actions.botMessage, room, message);
  }

  onGameEnd(result: GameResult[], room: Room): void {
    this.storage.clearRoomBotTimer(room);
    const message: string = this.messageService.get(messageTypes.complete, result);
    this.actionService.emitToRoom(actions.botMessage, room,  message);
  }
}
