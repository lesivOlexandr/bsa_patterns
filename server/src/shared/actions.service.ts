import { actions } from "../types/actions";
import { Socket } from 'socket.io';
import { Room} from "../types";

export class ActionsService {
  constructor(private readonly socket: Socket) {}

  emitToAll(action: actions, data: any) {
    this.socket.server.emit(action, data);
  }

  emitToRoom(action: actions, room: Room, data: any) {
    this.socket.server.to(room.name).emit(action, data);
  }

  emitToUser(action: actions, data: any) {
    this.socket.emit(action, data);
  }

  joinToRoom(roomName: string): void {
    this.socket.join(roomName);
  }

  leaveRoom(roomName: string): void {
    if (this.socket.in(roomName)){
      this.socket.leave(roomName);
    }
  }
}
