import { Room, User, roomStates, Progress } from "../types";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../socket/config";

export class DataStorage {
  private rooms: Room[] = [];
  private connectedUsers: Set<string> = new Set;
  private userRoomMapping: Map<string, Room> = new Map;
  private roomIntervalMapping: Map<string, NodeJS.Timeout> = new Map;
  private roomBotIntervalMapping: Map<string, NodeJS.Timeout> = new Map;

  public getRoomByName(name: string): Room | undefined {
    const room: Room | undefined = this.rooms.find(room => room.name === name);
    if (room) {
      return room;
    }
    return undefined;
  };

  /** 
   * @returns true if user successfully connected
  */
  public connectUser(username: string): boolean {
    if (this.connectedUsers.has(username)) {
      return false;
    }
    this.connectedUsers.add(username);
    return true;
  }

  /** 
   * @returns true if user successfully disconnected
  */
  public disconnectUser(username: string): boolean {
    return this.connectedUsers.delete(username);
  }

  public addRoom(room: Room): Room {
    this.rooms.push(room);
    return room;
  };

  public getRooms(): Room[] {
    return [...this.rooms];
  }

  public userHasRoom(username: string): boolean {
    return this.userRoomMapping.has(username);
  };

  public getUserRoom(username: string): Room | undefined {
    return this.userRoomMapping.get(username);
  }

  public removeUserFromRoom(username: string): boolean {
    const userRoom: Room | undefined = this.userRoomMapping.get(username);
    const success: boolean = this.userRoomMapping.delete(username);
    // if for some reasons room has no user with this name, then return false
    if (!success) {
      return success;
    };
    userRoom!.users = userRoom!.users.filter(roomUser => roomUser.name !== username);
    if (userRoom!.users.length < MAXIMUM_USERS_FOR_ONE_ROOM) {
      userRoom!.maxUsersReached = false;
    }
    return true;
  }

  public checkRoomActiveness(roomToCheck: Room): boolean {
    const room: Room | undefined = this.rooms.find(room => room.name === roomToCheck.name);
    if (room && !room.users.length) {
      this.rooms = this.rooms.filter(oneRoom => oneRoom !== room);
      return false;
    }
    return true;
  }

  public dropRoomState(roomName: string): Room | undefined {
    const room: Room | undefined = this.rooms.find(room => room.name === roomName);
    if (room) {
      room.text = undefined;
      room.users.forEach(user => user.progress = new Progress);
      room.state = roomStates.pending;
    }
    return undefined;
  }

  public addUserToRoom(user: User, roomName: string): boolean {
    const room: Room | undefined = this.getRoomByName(roomName);
    if (!room) {
      return false;
    }
    room.users.push(user);
    if (room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
      room.maxUsersReached = true;
    }
    this.userRoomMapping.set(user.name, room);
    return true;
  }

  public setRoomTimerPair(room: Room, timer: NodeJS.Timeout) {
    return this.roomIntervalMapping.set(room.name, timer);
  }
  
  public clearRoomTimer(room: Room): boolean {
    const timeoutId: NodeJS.Timeout | undefined = this.roomIntervalMapping.get(room.name);
    if (timeoutId) {
      clearTimeout(timeoutId);
      this.roomIntervalMapping.delete(room.name);
      return true;
    }
    return false;
  }

  public setRoomBotInterval(room: Room, timer: NodeJS.Timeout) {
    return this.roomBotIntervalMapping.set(room.name, timer);
  }

  public clearRoomBotTimer(room: Room): boolean {
    const timerId: NodeJS.Timeout | undefined = this.roomBotIntervalMapping.get(room.name);
    if (timerId) {
      clearInterval(timerId);
      this.roomBotIntervalMapping.delete(room.name);
      return true;
    }
    return false;
  }
}

export const dataStorage = new DataStorage;