import { Server as ioServer, Socket } from 'socket.io';
import { Routing } from '../shared/router.service';

export default (io: ioServer) => {
  io.on("connection", (socket: Socket) => new Routing(socket));
};
