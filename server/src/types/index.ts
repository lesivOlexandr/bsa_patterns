import { actions } from "./actions";

export interface ActionEmitter {
  emit: (action: actions, data: any, roomName: string) => void;
}

export class Room {
  constructor(
    public name: string,
  ){};

  public text?: string;
  public users: User[] = [];
  public maxUsersReached: boolean = false;
  public state: roomStates = roomStates.pending;
  public timeStarted: number = 0;
}

export enum roomStates {
  pending,
  timerStarted,
  gameStarted
}

export class Progress {
  constructor(
    public isReady: boolean = false,
    public inputtedText: string = '',
    public doneInPercents: number = 0,
    public spentSeconds: number = 0
  ){}
};

export class User {
  public progress: Progress = new Progress;
  constructor(public name: string){}
}

export interface GameResult {
  username: string,
  spentSeconds: number | null
}

export interface MessageFormatterFunc {
  (data: any): string;
}