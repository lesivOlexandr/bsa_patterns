import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";

const router: Router = Router();

router
  .get("/", (_, res) => {
    const page: string = path.join(HTML_FILES_PATH, "login.html");
    res.sendFile(page);
  });

export default router;
